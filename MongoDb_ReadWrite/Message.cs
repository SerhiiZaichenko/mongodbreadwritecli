﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace MongoDb_ReadWrite
{
    public class Message
    {

        [BsonId]
        public ObjectId Id { get; set; }
        public string Category { get; set; }
        public string Text { get; set; }
        public Message(string category, string text)
        {
            Category = category;
            Text = text;
        }
    }
}
