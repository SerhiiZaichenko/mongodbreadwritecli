﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace MongoDb_ReadWrite
{
    public class MongoReadWriteCLI
    {
        readonly IMongoCollection<Message> collection;
        public MongoReadWriteCLI(string connString)
        {
            var client = new MongoClient(connString);
            collection = client.GetDatabase("test").GetCollection<Message>("Messages");

        }
        public void Start()
        {
            Console.WriteLine(Help());
            string cmdLine = null;
            while (cmdLine != "end")
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write("Write command: ");
                Console.ForegroundColor = ConsoleColor.White;
                cmdLine = Console.ReadLine();
                string[] cmd = cmdLine.Split(' ');
                switch (cmd.FirstOrDefault())
                {
                    case "Write":
                        if (cmd.Length < 3)
                        {
                            Console.WriteLine("Wrong number of args for write");
                        }
                        else
                        {
                            try
                            {
                                var message=WriteMessage(cmd[1], string.Join(" ", cmd, 2, cmd.Length - 2));
                                Console.WriteLine("Message succesfully saved: Category:" + message.Category + " Text:" +message.Text);
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("Exception occured while adding to database:" + ex.Message);
                            }
                        }
                        break;
                    case "Read":
                        string textFilter = "";
                        string categoryFilter = "";
                        if (cmd.Length == 5 && cmd[1] == "/c" && cmd[3] == "/t")
                        {
                            textFilter = cmd[4];
                            categoryFilter = cmd[2];

                        }
                        else if (cmd.Length == 3)
                        {
                            if (cmd[1] == "/c")
                                categoryFilter = cmd[2];
                            if (cmd[1] == "/t")
                                textFilter = cmd[2];
                        }
                        else if (cmd.Length != 1)
                        {
                            Console.WriteLine("Wrong args");
                            break;
                        }
                        try
                        {
                            var messages = FindByFilters(textFilter, categoryFilter);
                            if (messages.Count == 0)
                                Console.WriteLine("No messages found by the specified filters");
                            foreach (Message message in messages)
                            {
                                Console.WriteLine("Category:" + message.Category + " Text:" + message.Text);
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Exception occured while reading from database:" + ex.Message);
                        }
                        break;
                    default:
                        Console.WriteLine("Wrong command,try one of:");
                        Console.WriteLine(Help());
                        break;
                }
            }
        }
        private string Help()
        {
            return "Write <Category> <Text>\n\tAdds new message\n " +
              "\tExample:\n\t\tWrite Important Lorem ipsum dolor sit amet" +
              "\nRead [/c <CategoryFilter>] [/t <TextFilter>] \n\tFind messages by specified filter" +
              "\n\tExamples:\n\t\tRead\n\t\tRead /t ipsum\n\t\tRead /c Important\n\t\tRead /c Important /t iPsUm";
        }
        private List<Message> FindByFilters(string textFilter, string categoryFilter)
        {
            FilterDefinition<Message> filter = FilterDefinition<Message>.Empty;
            if (textFilter != "")
            {
                var textRegEx = new BsonRegularExpression(new Regex(textFilter, RegexOptions.IgnoreCase));
                filter &= Builders<Message>.Filter.Regex("Text", textRegEx);
            }
            if (categoryFilter != "")
            {
                var categoryRegEx = new BsonRegularExpression(new Regex("^" + categoryFilter + "$", RegexOptions.None));
                filter &= Builders<Message>.Filter.Regex("Category", categoryRegEx);
            }
            var messages = collection.Find(filter).ToList();
            return messages;
        }

        private Message WriteMessage(string category, string text)
        {
            var message = new Message(category, text);
            collection.InsertOne(message);
            return message;            
        }

    }

}
