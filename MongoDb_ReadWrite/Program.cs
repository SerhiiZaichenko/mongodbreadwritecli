﻿namespace MongoDb_ReadWrite
{
    static class Program
    {
        static void Main(string[] args)
        {
            var cli = new MongoReadWriteCLI("mongodb://127.0.0.1:27017/?compressors=disabled&gssapiServiceName=mongodb");
            cli.Start();
        }
    }
}
